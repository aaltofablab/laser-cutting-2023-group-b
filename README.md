# Laser Cutting 2023 Group B
The computer crached in my first trial of writing this and I lost all the text so here is the second trial!

Kerf is the error in the cut due to the laser beam diameter. Below I will explain how we calculated kerf using 5 squares in the demonstration session.

- In order to calculate kerf one needs to select some simple shapes like squares, their size (such as 20x20 mm in our demo case) and the amount of samples to be averaged upon (such as 5 in our demo case).
- Then we can open Adobe Illustrator (AI) and start drawing these equivalent squares in a row using the recangle took and CTRL. 

![Adobe Illustrator Squares](AI.jpg)

    - Tip 1: In case of having a drawing made in DXF format, you can select the place option in AI from edit menu and please also enable the import options button while doing so. After this you will have the option to choose the scaling, which you should select original size and scale the linework as well enabled.
    - Tip 2: If you have already drawn the file in another computer in AI, just move this file with a USB stick and open this up in AI in the laser cutter computer. 
- After this we need to set the stroke thickness of the drawings to 0.01 mm. 
    - Tip 3: This number is important, because this is how the laser cutter will understand to cut instead of ignoring the lines.
    - Tip 4: In order to engrave, do not have strokes just fill. For example if you have a text turn it into object and just have a fill wihtout strokes, then the laser cutter software will recognize it as engraving.
- If you wish at this step you can save the file for further use or adjustments.
- After this select edit and print, and here select the Epilog Laser CuttSer device as the printer. (You need to be on the laser cutter computer for this).
    - Tip 5: By the time we arrive to this step, you should have turned on the ventilation, checked the fire safety situation, activate the laser cutter by showing the card on the table to the log device, click OK, rotate the manual key on the laser cutter, wait for the laser cutter screen to load, insert your material in and manually focus the laser head onto your material (preferebly at the bottom left corner, since this is the worst cutting part of the machine). I won't go to further detail on this because I believe this is not relevant here.
- At this stage the Epilog Laser Cutter software will open up, and you should see the camera view and your square pieces from the laser cutter.
    - Tip 6: If you do not see the full camera view at this point, please restart the laser cutter.
- In this window, move the square pieces to a location on the material, where you want to cut them from.
- Also in the same window select the type and thickness of the material from the folder button on the right top. You would have different settings for cutting and engraving.
- Here when you select the correct material and thickness the recommmended settings of the laser for engraving and cutting will be set.
    - Tip 7: Although these should be adjusted depending on design, laser cutter condition, and the location in the cutting area could effect these, so it is recommended to do some test pieces to adjust these setting before deciding the final values and kerf test.
    - Tip 8:

|Settings|Speed|Power|Frequency|
|---|---|---|---|
|**Meaning**|Speed of laser moving over the design|Power of the laser source|Frequency of laser beam shots|
|**Larger value means**|Less contact time to cut/engrave|More powerful cut/engraving (potential of burning in some materials)|More shots per part of the design potentially deeper cut|

- Then select either print or send to Job Manager(JM). If you select the print option, the job will directly be sent to the screen of the laser cutter. If you select JM, a new window will open with JM software. Here you need to click on Jobs, select the correct job and select quick print form the top right corner.
    - Tip 9: Using JM allows you to access the job later to modify or re-use.
- At this point you job should be sent to the laser cutter screen.
- Select the sent job from the screen and press play button, this will start the cutting process.
    - Tip 10: The laser cutter screen displays the estimated duration of the cutting job.
- After the cut is done remove the square from the laser cutters mouth.

![Square Cuts](square_cuts.jpg)

- Coming back to kerf measurement:
    - Align all squares in a row.
    - Measure the length with a caliper.
    - For the calculations:
        - We wanted to have 5 pieces x 20 mm = 100 mm length.
        - Although we have measured 98.6 mm length.

![Measurement 1](measurement1.jpg)

        - The difference between these two values will give us the total error = 1.4 mm.
        - The total error divided to number of samples (5) will give us the average error for each sample, which is the kerf = 0.28 mm.
- After calculating the kerf we need to account for this kerf within our design to get precision cuts.
- For including this we can extend the squares from both sides by kerf/2 mm.
        - Tip 11: Imagine the laser beam as a circle with the diameter of kerf.

![Kerf Adjustment](kerf_adjustment.jpg)

- We can now recut the adjusted 5 squares following the steps above.
- When we measure the new length we see that it is closer to 100 mm (100.3 mm).

![MEasurement 2](measurement2.jpg)

- If wanted you can introduce one mroe variable to adjust the tightness of the fit.
    
    




    